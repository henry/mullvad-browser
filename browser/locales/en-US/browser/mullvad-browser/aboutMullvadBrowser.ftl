about-mullvad-browser-heading = { -brand-product-name }
about-mullvad-browser-developed-by = Developed in collaboration between the <a data-l10n-name="tor-project-link">Tor Project</a> and <a data-l10n-name="mullvad-vpn-link">Mullvad VPN</a>
about-mullvad-browser-use-vpn = Get more privacy by using the browser <a data-l10n-name="with-vpn-link">with Mullvad VPN</a>.
about-mullvad-browser-learn-more = Curious to learn more about the browser? <a data-l10n-name="learn-more-link">Take a dive into the mole hole</a>.


## Deprecated. To be removed when 13.5 becomes stable.

about-mullvad-browser-page-title = { -brand-product-name } Home
